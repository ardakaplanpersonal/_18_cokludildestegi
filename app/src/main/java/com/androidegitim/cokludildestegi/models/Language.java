package com.androidegitim.cokludildestegi.models;

/**
 * Created by Arda Kaplan on 5.01.2018 - 14:56
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public enum Language {

    ENGLISH("English", "en"),
    TURKISH("Türkçe", "tr"),
    FRENCH("Français", "fr");

    private String screenName;
    private String deviceName;

    Language(String screenName, String deviceName) {
        this.screenName = screenName;
        this.deviceName = deviceName;
    }

    public String getScreenName() {
        return screenName;
    }

    public String getDeviceName() {
        return deviceName;
    }
}
