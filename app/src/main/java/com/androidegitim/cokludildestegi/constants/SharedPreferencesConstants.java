package com.androidegitim.cokludildestegi.constants;

/**
 * Created by Arda Kaplan on 5.01.2018 - 15:25
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public final class SharedPreferencesConstants {

    public static final String APPLICATION_LANGUAGE = "APPLICATION_LANGUAGE";

    private SharedPreferencesConstants() {

    }
}