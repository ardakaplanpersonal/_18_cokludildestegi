package com.androidegitim.cokludildestegi.ui.adapters;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidegitim.androidegitimlibrary.helpers.ApplicationHelpers;
import com.androidegitim.androidegitimlibrary.helpers.SharedPreferencesHelpers;
import com.androidegitim.cokludildestegi.constants.SharedPreferencesConstants;
import com.androidegitim.cokludildestegi.models.Language;
import com.androidegitim.cokludildestegi.R;
import com.androidegitim.cokludildestegi.ui.activites.MainActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Arda Kaplan on 5.01.2018 - 14:52
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class LanguagesRecyclerViewAdapter extends RecyclerView.Adapter<LanguagesRecyclerViewAdapter.LanguageViewHolder> {

    private Activity activity;
    private List<Language> languageArrayList;

    public LanguagesRecyclerViewAdapter(Activity activity, List<Language> languageArrayList) {
        this.activity = activity;
        this.languageArrayList = languageArrayList;
    }

    @Override
    public LanguageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

        View view = layoutInflater.inflate(R.layout.row_language, parent, false);

        return new LanguageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(LanguageViewHolder holder, int position) {

        final Language language = languageArrayList.get(position);

        holder.languageNameTextView.setText(language.getScreenName());

        holder.rootLinearLayout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                ApplicationHelpers.changeLanguage(activity, language.getDeviceName());

                SharedPreferencesHelpers.putString(activity, SharedPreferencesConstants.APPLICATION_LANGUAGE, language.getDeviceName());

                activity.onBackPressed();
            }
        });
    }

    @Override
    public int getItemCount() {
        return languageArrayList.size();
    }

    class LanguageViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.row_language_linearlayout_root)
        LinearLayout rootLinearLayout;
        @BindView(R.id.row_language_textview_name)
        TextView languageNameTextView;


        LanguageViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
