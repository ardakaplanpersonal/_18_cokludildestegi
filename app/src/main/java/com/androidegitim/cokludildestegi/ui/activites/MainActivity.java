package com.androidegitim.cokludildestegi.ui.activites;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Toast;

import com.androidegitim.androidegitimlibrary.helpers.ApplicationHelpers;
import com.androidegitim.androidegitimlibrary.helpers.SharedPreferencesHelpers;
import com.androidegitim.cokludildestegi.R;
import com.androidegitim.cokludildestegi.constants.SharedPreferencesConstants;
import com.androidegitim.cokludildestegi.models.Language;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

//  android de kullanılan diller ve kullanılış şekilleri
//    https://github.com/championswimmer/android-locales


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();

        checkLanguage();

        setViews();
    }

    private void setViews() {

        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
    }

    private void checkLanguage() {

        String userSelectedLocale = SharedPreferencesHelpers.getString(this, SharedPreferencesConstants.APPLICATION_LANGUAGE);

        if (!userSelectedLocale.equals("")) {

            ApplicationHelpers.changeLanguage(this, userSelectedLocale);
        }
    }


    @OnClick(R.id.main_button_change_language)
    public void openChangeLanguageActivity() {

        startActivity(new Intent(MainActivity.this, LanguagesActivity.class));
    }
}
