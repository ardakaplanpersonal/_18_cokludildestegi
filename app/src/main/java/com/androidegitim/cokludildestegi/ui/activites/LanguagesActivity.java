package com.androidegitim.cokludildestegi.ui.activites;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.androidegitim.cokludildestegi.models.Language;
import com.androidegitim.cokludildestegi.ui.adapters.LanguagesRecyclerViewAdapter;
import com.androidegitim.cokludildestegi.R;

import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Arda Kaplan on 5.01.2018 - 14:43
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class LanguagesActivity extends AppCompatActivity {

    @BindView(R.id.languages_recycler_view)
    RecyclerView languagesRecyclerView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_languages);

        ButterKnife.bind(this);

        fillList();
    }

    private void fillList() {

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());

        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        languagesRecyclerView.setLayoutManager(linearLayoutManager);


        languagesRecyclerView.setAdapter(new LanguagesRecyclerViewAdapter(LanguagesActivity.this, Arrays.asList(Language.values())));

    }
}
