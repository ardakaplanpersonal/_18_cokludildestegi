package com.androidegitim.androidegitimlibrary.helpers;

import android.app.Activity;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;

import java.util.Locale;

/**
 * Created by Arda Kaplan on 5.01.2018 - 15:21
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public final class ApplicationHelpers {

    private ApplicationHelpers() {

    }

    public static void changeLanguage(Activity activity, String localeShortName) {

        Locale locale = new Locale(localeShortName);

        Locale.setDefault(locale);

        Resources resources = activity.getResources();

        Configuration configuration = new Configuration(resources.getConfiguration());

        configuration.locale = locale;

        resources.updateConfiguration(configuration, resources.getDisplayMetrics());
    }
}
