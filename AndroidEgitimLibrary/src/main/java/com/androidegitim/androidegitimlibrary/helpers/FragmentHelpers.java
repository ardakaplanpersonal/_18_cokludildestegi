package com.androidegitim.androidegitimlibrary.helpers;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by Arda Kaplan on 2.01.2018 - 10:31
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public final class FragmentHelpers {

    private FragmentHelpers() {

    }

    public static void openFragment(AppCompatActivity activity, Fragment openingFragment, int fragmentLayoutID) {

        FragmentTransaction fragmentTransaction = activity.getSupportFragmentManager().beginTransaction();

//        fragmentTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left);

        fragmentTransaction.replace(fragmentLayoutID, openingFragment);

        fragmentTransaction.addToBackStack(openingFragment.getClass().getName());

        fragmentTransaction.commit();
    }
}
